//
//  Trailer.swift
//  CloneNetflix
//
//  Created by entymon on 24/01/2021.
//

import Foundation

struct Trailer: Identifiable, Hashable {
    var id: String = UUID().uuidString
    var name: String
    var videoURL: URL
    var thumbnailImageURL: URL
}
