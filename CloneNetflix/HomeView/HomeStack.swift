//
//  HomeStack.swift
//  CloneNetflix
//
//  Created by Pawel Olejniczak on 29/01/2021.
//

import SwiftUI

struct HomeStack: View {
    var vm: HomeViewModel = HomeViewModel()
    
    var topRowSelection: HomeTapRow
    
    @Binding var movieDetailToShow: Movie?
    
    var body: some View {
        ForEach(vm.allCategories, id: \.self) { category in
            VStack {
                HStack {
                    Text(category)
                        .font(.title)
                        .bold()
                    Spacer()
                }
                
                ScrollView(.horizontal, showsIndicators: false) {
                    LazyHStack {
                        ForEach(vm.getMovies(forCategory: category, andHomeRow: topRowSelection)) { movie in
                            StandardHomeMovie(movie: movie)
                                .frame(width: 100, height: 200)
                                .padding(.horizontal, 10)
                                .onTapGesture {
                                    perform: do {
                                        movieDetailToShow = movie
                                    }
                                }
                        }
                    }
                }
            }
        }
    }
}


struct HomeStack_Previews: PreviewProvider {
    static var previews: some View {
        ZStack {
            Color.black
                .edgesIgnoringSafeArea(.all)
            
            ScrollView {
                HomeStack(topRowSelection: .movies, movieDetailToShow: .constant(nil))
            }
            .foregroundColor(.white)
        }
    }
}
