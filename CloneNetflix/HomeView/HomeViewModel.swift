//
//  HomeViewModel.swift
//  CloneNetflix
//
//  Created by entymon on 09/01/2021.
//

import Foundation

class HomeViewModel: ObservableObject {
    
    @Published var movies: [String: [Movie]] = [:]
    
    public var allCategories: [String] {
        movies.keys.map({ String($0) })
    }
    
    public func getMovies(forCategory category: String, andHomeRow homeRow: HomeTapRow) -> [Movie] {
        
        switch homeRow {
        case .home:
            return movies[category] ?? []
        case .movies:
            return (movies[category] ?? []).filter({ $0.movieType == .movie })
        case .tvShows:
            return (movies[category] ?? []).filter({ $0.movieType == .tvShow })
        case .myList:
            return movies[category] ?? []
            // TODO: setup my list properly
        }
    }
    
    init() {
        setUpMovies()
    }
    
    func setUpMovies() {
        movies["Trending Now"] = exampleMovies
        movies["Stand-Up Comedy"] = exampleMovies.shuffled()
        movies["Watched Again"] = exampleMovies.shuffled()
        movies["Sci-Fi Movies"] = exampleMovies.shuffled()
        movies["New Release"] = exampleMovies.shuffled()
    }
}
