//
//  StandardHomeMovie.swift
//  CloneNetflix
//
//  Created by entymon on 09/01/2021.
//

import SwiftUI
import Kingfisher

struct StandardHomeMovie: View {
    var movie: Movie
    
    
    var body: some View {
        KFImage(movie.thumbnailURL)
            .resizable()
            .scaledToFit()
    }
}

struct StandardHomeMovie_Previews: PreviewProvider {
    static var previews: some View {
        StandardHomeMovie(movie: exampleMovie1)
    }
}
