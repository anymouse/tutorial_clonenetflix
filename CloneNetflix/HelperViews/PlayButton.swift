//
//  WhiteButton.swift
//  CloneNetflix
//
//  Created by entymon on 10/01/2021.
//

import SwiftUI

struct PlayButton: View {
    
    var text: String
    var imageName: String
    var backgroundColor: Color = Color.white
    
    
    var action: () -> Void // this is at the end of the list because can be used as clousure
    
    var body: some View {
        Button(action: action, label: {
            HStack {
                Spacer()
            
                Image(systemName: imageName)
                    .font(.headline)
                
                Text(text)
                    .bold()
                    .font(.system(size: 16))
                
                Spacer()
            }
            .padding(.vertical, 6)
            .foregroundColor(backgroundColor == .white ? .black : .white)
            .background(backgroundColor)
            .cornerRadius(/*@START_MENU_TOKEN@*/3.0/*@END_MENU_TOKEN@*/)
        })
    }
}

struct WhiteButton_Previews: PreviewProvider {
    static var previews: some View {
        ZStack {
            Color.black
            
            PlayButton(text: "Play", imageName: "play.fill") {
                print("tapped")
            }
        }
    }
}
