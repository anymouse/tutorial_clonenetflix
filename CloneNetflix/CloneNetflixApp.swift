//
//  CloneNetflixApp.swift
//  CloneNetflix
//
//  Created by entymon on 09/01/2021.
//

import SwiftUI

@main
struct CloneNetflixApp: App {
    var body: some Scene {
        WindowGroup {
            HomeView()
        }
    }
}
