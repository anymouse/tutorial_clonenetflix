//
//  TopMoviePreview.swift
//  CloneNetflix
//
//  Created by entymon on 10/01/2021.
//

import SwiftUI
import Kingfisher

struct TopMoviePreviewView: View {
    var movie: Movie
    
    var vm: TopMoviePreviewViewModel = TopMoviePreviewViewModel()
    
    var body: some View {
        ZStack {
            KFImage(movie.thumbnailURL)
                .resizable()
                .scaledToFill()
                .clipped()
            
            VStack {
                Spacer()
                
                HStack {
                    ForEach(movie.categories, id: \.self) { category in
                        Text(category)
                            .font(.system(size: 11))
                        
                        if !vm.isCategoryLast(movie, category) {
                            Image(systemName: "circle.fill")
                                .foregroundColor(/*@START_MENU_TOKEN@*/.blue/*@END_MENU_TOKEN@*/)
                                .font(.system(size: 3))
                        }
                    }
                }
                .padding(.bottom, 1)
                
                HStack {
                    Spacer()
                    
                    SmallVerticalButtonView(text: "My List", isOnImage: "checkmark", isOffImage: "plus", isOn: true) {
                        //
                    }
                    
                    Spacer()
                    
                    PlayButton(text: "play", imageName: "play.fill") {
                        //
                    }
                    .frame(width: 120)
                    
                    Spacer()
                    
                    SmallVerticalButtonView(text: "Info", isOnImage: "info.circle", isOffImage: "info.circle", isOn: true) {
                        //
                    }
                    
                    Spacer()
                }
            }
            .background(
                LinearGradient.blackOpacityGradient
                    .padding(.top, 250)
            )
        }
        .foregroundColor(.white)
    }
}

struct TopMoviePreviewView_Previews: PreviewProvider {
    static var previews: some View {
        TopMoviePreviewView(movie: exampleMovie1)
    }
}
