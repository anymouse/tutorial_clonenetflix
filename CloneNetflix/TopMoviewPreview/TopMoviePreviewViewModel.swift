//
//  TopMoviePreviewViewModel.swift
//  CloneNetflix
//
//  Created by entymon on 10/01/2021.
//

import Foundation

class TopMoviePreviewViewModel: ObservableObject {
    
    func isCategoryLast(_ movie: Movie, _ category: String) -> Bool {
        let categoryCount = movie.categories.count // 4
        
        if let index = movie.categories.firstIndex(of: category) {
            if index + 1 != categoryCount {
                return false
            }
        }
        return true
    }
}
